import os
import subprocess

websocket_reporter = "C:\\Users\\hoggit\\DCSUTILS\\Server_Scripts\\WebhookAlerter\\webhookAlerter.ps1"

is_debug = None
websocket_reporter_enabled = None
metar_outfile = None


def utils_setup(_is_debug, _websocket_reporter_enabled, _metar_outfile):
    global is_debug, websocket_reporter_enabled, metar_outfile

    is_debug = _is_debug
    websocket_reporter_enabled = _websocket_reporter_enabled
    metar_outfile = _metar_outfile


def debug(format, *args, **kwargs):
    if is_debug:
        log(format, *args, **kwargs)


def log(format, *args, **kwargs):
    print(format, *args, **kwargs)


def webhook_report(format, *args, **kwargs):
    if websocket_reporter_enabled:
        message = format.format(*args, **kwargs)
        subprocess.run(["powershell.exe", websocket_reporter, "\"error\"", "\"mission splitter\"", "\"{}\"".format(message)])


def metar_out(code):
    if metar_outfile:
        debug("metar outfile arg provided: {}".format(metar_outfile))
        abs_path = os.path.abspath(metar_outfile)
        path = os.path.dirname(abs_path)
        log("path {}".format(path))
        if not os.path.exists(path):
            os.makedirs(path)
        with open(abs_path, 'w', encoding='utf-8') as mf:
            mf.write(code)

import datetime
import dcs

from dcs import Mission

from dcs_mission_time_split.utils import *


def apply_weather(mission: Mission, weather_dict):
    mission.weather.clouds_thickness = weather_dict['cloud_height']
    mission.weather.clouds_density = weather_dict['cloud_density']
    mission.weather.clouds_base = weather_dict['cloud_base']
    mission.weather.clouds_iprecptns = dcs.weather.Weather.Preceptions(weather_dict['precip'])
    mission.weather.qnh = weather_dict['pressure']
    mission.weather.season_temperature = weather_dict['temp']
    mission.weather.wind_at_ground.direction = weather_dict['wind_dir']
    mission.weather.wind_at_ground.speed = weather_dict['wind_speed']

    mission.weather.wind_at_2000.direction = weather_dict['wind_dir']
    mission.weather.wind_at_2000.speed = weather_dict['wind_speed']

    mission.weather.wind_at_8000.direction = weather_dict['wind_dir']
    mission.weather.wind_at_8000.speed = weather_dict['wind_speed']


def apply_timeslot(mission: Mission, time_delta: datetime.timedelta):
    today = datetime.datetime.now()
    mission.start_time = datetime.datetime(year=today.year, month=today.month, day=today.day) + time_delta


def process_mission(filename, destination_folder, time_slots, weather_dict):
    mission = dcs.Mission()
    mission.load_file(filename)

    for slot_name, slot_time_delta in time_slots.items():
        apply_weather(mission, weather_dict)
        apply_timeslot(mission, slot_time_delta)

        mission.save(os.path.join(destination_folder, "{}_{}.miz".format(filename[:-4], slot_name)))

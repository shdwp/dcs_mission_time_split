import requests
import random
import sys

from metar import Metar

from dcs_mission_time_split.utils import *


def cloud_map(sky):
    """
    Takes a list of tuples indicating cloud cover, generated from the Metar egg, and
    returns a list of tuples translated into DCS Cloud Cover and base height.

    Incoming Format ('BKN', <distance>, <something>)
    Outgoing Format ( 8, distance.value())

    If <distance> is Nil or falsey, we replace it with zero.
    The Outgoing format looks up the cloud density in the cloud_map var to determine the thickness between 0-10.
    """
    cloud_map = {
        'NSC': 0,
        'NCD': 0,
        'CLR': 0,
        'FEW': 2,
        'SCT': 6,
        'BKN': 8,
        'OVC': 10
    }
    return list(map(lambda s: (cloud_map[s[0]], s[1].value() if s[1] else 0), sky))

def thickest_clouds(cloud_thickness_and_base_list):
    """
    Given a list of tuples indicated cloud thickness and base, return the tuple
    with the thickest clouds
    """
    return max(cloud_thickness_and_base_list, key=lambda c: c[0])

def get_cloud_detail(sky):
    """
    Pull the thickest clouds from the Metar's sky list and return a dictionary
    with the following keys:
    "thickness": The cloud's thickness from 0-10 (for DCS).
    "base": the base height of the clouds
    """
    debug("Getting cloud details")
    clouds = cloud_map(sky)
    debug("There are {} clouds listed in the Metar".format(len(clouds)))
    thickest = thickest_clouds(clouds)
    debug("Found thickest clouds: thick: {} -- base {}".format(thickest[0], thickest[1]))
    return {
            "thickness": thickest[0],
            "base": thickest[1]
            }

def wind_speed_in_mps(wind):
    """
    Given a wind_speed object from a Metar, return the windspeed in meters per second format.
    """
    if wind._units == "KT":
        return wind.value() / 1.944
    if wind._units == "KPH":
        return wind.value() / 3.6
    if wind._units == "MPH":
        return wind.value() / 2.237
    return wind.value()


def retrieve_weather_dict(weatherconf, fallback):
    def check_fallback():
        if not fallback:
            print("Fallback flag not specified, quitting.")
            sys.exit(1)
        else:
            print("Falling back to defaults")

    wx = {
        "temp": 23,
        "wind_speed": 4,
        "wind_dir": random.randint(250, 280),
        "cloud_base": 8000,
        "cloud_height": 1800,
        "cloud_density": 5,
        "precip": 0,
        "pressure": 760
    }
    try:
        authHeader = {"Authorization": weatherconf.authToken}
        wx_request = requests.get("https://avwx.rest/api/metar/" + weatherconf.icao.upper(), headers=authHeader, timeout = 5)
        if wx_request.status_code == 200:
            try:
                wx_json = wx_request.json()
                obs = Metar.Metar(wx_json['raw'], strict=False)

                # obs = Metar.Metar("OMDB 041600Z 28010KT 250V310 9999 BKN036 21/12 Q1014 NOSIG")
                precip = 0
                if obs.weather:
                    if obs.weather[0][2] == 'RA':
                        precip = 1
                    if obs.weather[0][1] == 'TS':
                        precip = 2

                wx['temp'] = obs.temp.value()
                wx['wind_speed'] = wind_speed_in_mps(obs.wind_speed)
                if obs.wind_dir:
                    wx['wind_dir'] = (obs.wind_dir.value() + 180) % 360
                if obs.sky:
                    clouds = get_cloud_detail(obs.sky)
                    wx['cloud_base'] = clouds["base"] * 0.3048 #METAR is Feet, Miz file expects meters
                    wx['cloud_height'] = 1800  * 0.3048 #METAR is Feet, Miz file expects meters
                    wx['cloud_density'] = clouds["thickness"]
                else:
                    wx['cloud_base'] = 1800
                    wx['cloud_height'] = 1800
                    wx['cloud_density'] = 0
                wx['precip'] = precip
                wx['pressure'] = obs.press.value() / 1.33

                debug("----------------")
                debug(obs.code)
                debug("----------------")
                metar_out(obs.code)
            except Exception as e:
                debug(e)
                message = "FAILED TO GET DYNAMIC WEATHER"
                log(message)
                webhook_report(message)
                check_fallback()
        else:
            debug(wx_request)
            message = "FAILED TO GET DYNAMIC WEATHER. METAR API UNAVAILABLE"
            log(message)
            webhook_report(message)
            check_fallback()
    except Exception as e:
        debug(e)
        message = "Could not contact avwx for weather."
        log(message)
        webhook_report(message)
        check_fallback()

    return wx

class WeatherConfig:
    def __init__(self, icao, authToken):
        self.icao = icao
        self.authToken = authToken

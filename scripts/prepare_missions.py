#!/usr/local/bin/python3
import argparse

from datetime import timedelta
from dcs_mission_time_split.utils import *
from dcs_mission_time_split import mts, mission_handler

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Split your DCS mission into different times, with weather from avwx")
    parser.add_argument('-m', '--mission', required=True, help="The mission you want to split")
    parser.add_argument('-i', '--icao', help="The ICAO designation of the airport to get weather for")
    parser.add_argument('--metarout', help="Output the METAR string to this file. Ignored if not set")
    parser.add_argument('-f', '--fallback', action='store_true',
                        help="Add this if you want to fall back to a default weather if no ICAO is found.\
                            If not specified, and no ICAO weather is found, we'll exit without doing anything")
    parser.add_argument('-o', '--output', default=None, help="The directory to output the split missions to. Defaults to the current directory.")
    parser.add_argument('-d', '--debug', action='store_true', help="More debug output")
    parser.add_argument('--webhook-alerter', default=True, help='Enable/disable webhook alerter.')
    parser.add_argument('-A', '--avwx_auth', help="Authentication to avwx.rest")

    args = parser.parse_args()
    utils_setup(args.debug, args.webhook_alerter, args.metarout)

    time_slots = {
        'morning': timedelta(hours=9),
        'afternoon': timedelta(hours=12),
        'evening': timedelta(hours=23),
    }
    weather_config = mts.WeatherConfig(args.icao, args.avwx_auth)
    weather_dict = mts.retrieve_weather_dict(weather_config, args.fallback)
    mission_handler.process_mission(args.mission, args.output or "", time_slots, weather_dict)
    log("Done")
